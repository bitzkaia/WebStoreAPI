﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace StoreAPI.Models
{

    public class Category
    {
        public int ID { get; set; }
        public string Name { get; set; }

        Product[] Products { get; set; }
    }

}
